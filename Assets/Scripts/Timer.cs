﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour {

    public double theTime;

    private bool paused = false;

    public bool isPassed(double seconds)
    {
        return (Time.deltaTime - theTime) > seconds;
    }

    public void startTimer()
    {
        paused = false;
        theTime = Time.deltaTime;
    }

    public void pause()
    {
        paused = true;
    }

    public bool isPaused()
    {
        return paused;
    }
}

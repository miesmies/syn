﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottomPanelUI : MonoBehaviour {

    public List<SoundScapeButton> ssbtns = new List<SoundScapeButton>();

    void Start()
    {
        foreach(SoundScapeButton i in GetComponentsInChildren<SoundScapeButton>())
        {
            ssbtns.Add(i);
        }
    }

    public void ToggleElement(GameObject obj)
    {
        if (obj.activeInHierarchy)
        {
            obj.SetActive(false);
        }
        else
        {
            obj.SetActive(true);
        }
    }

    //public void ClearButtons()
    //{
    //    foreach(SoundScapeButton i in ssbtns)
    //    {
    //        i.additionalButtons.SetActive(false);
    //    }
    //}
}

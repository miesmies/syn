﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorPickers : MonoBehaviour {

    public Color defaultColor;
    public Color drumColor;
    public Color midColor;
    public Color highColor;
    public Color enabledColor;
}

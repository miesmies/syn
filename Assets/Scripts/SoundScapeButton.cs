﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundScapeButton : MonoBehaviour {

    //public int index;
    public SoundScape soundScape;
    public Image[] buttonList;
    public Image img;
    public bool startKit = false;
    //public GameObject additionalButtons;
    private void Start()
    {
        img = GetComponent<Image>();
        if (!startKit)
        {
            Color color;
            color = img.color;
            color.a = 0.65f;
            img.color = color;
        }
    }

    public void SelectSoundScape()
    {
        //GameObject.Find("Bottom Panel").GetComponent<BottomPanelUI>().ClearButtons();
        //GameObject.Find("GameManager").GetComponent<LegacySeq>().currentSoundScape = soundScape;
        //LegacySeq.currentSoundScape = soundScape;
        //additionalButtons.SetActive(true);
        foreach(Image i in buttonList)
        {
            Color color;
            color = i.color;
            color.a = 0.65f;
            i.color = color;
        }
        Color colo;
        colo = img.color;
        colo.a = 1f;
        img.color = colo;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using FMODUnity;

public class Note : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler, IPointerUpHandler {

    public Color inactiveColor;
    public Color activeColor;

    public int index;
    public int row;

    public bool active = false;

    public Image img;
    private Animator anim;

    public Sequencer mySeq;

    public ColorPickers colors;

    public void Step()
    {
       
        if (active && LegacySeq.currentIndex == index)
        {
            Flash();
            //RuntimeManager.PlayOneShot("event:/" + LegacySeq.currentSoundScape.soundScapeName + "/" + LegacySeq.currentSoundScape.kitString[row]);
        }
        
    }

    private void Update()
    {
        //Debug.Log(ToRunInMainThread.Count);
        while(ToRunInMainThread.Count > 0)
        {
            Action func = ToRunInMainThread[0];
            ToRunInMainThread.RemoveAt(0);
            func();
        }
    }

    private void Start()
    {
        LegacySeq.OnStep += this.Step;
        img = GetComponent<Image>();
        anim = GetComponent<Animator>();
        colors = GameObject.Find("GameManager").GetComponent<ColorPickers>();
        if (row < 5)
        {
            img.color = colors.drumColor;
            inactiveColor = colors.drumColor;
        }
        else if (row > 4 && row < 13)
        {
            img.color = colors.midColor;
            inactiveColor = colors.midColor;
        }
        else
        {
            img.color = colors.highColor;
            inactiveColor = colors.highColor;
        }
        GameObject.Find("GameManager").GetComponent<LegacySeq>().notes.Add(this);
    }

    public List<Action> ToRunInMainThread = new List<Action>();
    public void QueueMainThreadAction(Action a)
    {
        ToRunInMainThread.Add(a);
    }
    public void Flash()
    {
        QueueMainThreadAction(FlashInternal);
        
    }
    public void FlashInternal()
    {
        
        anim.SetTrigger("Flash");
    }

    public void HighLight()
    {
        QueueMainThreadAction(HighLightInternal);    
        //HighLightInternal();
    } 

    private void HighLightInternal()
    {
        anim.SetTrigger("Highlight");
    }

    //private void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.A) && row == 5 && index == 5)
    //    {
    //        Debug.Log("flashed");
    //        Flash();
    //    }
    //}

    public void ToggleNote()
    {
        if (!active)
        {
            this.img.color = activeColor;
            mySeq.sequence[this.index] = true;
            active = true;
        }else
        {
            this.img.color = inactiveColor;
            mySeq.sequence[this.index] = false;
            active = false;
        }
    }

    public void DeactivateNote()
    {
       
            this.img.color = inactiveColor;
            mySeq.sequence[this.index] = false;
            active = false;
        
    }


    public static bool clicked;
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (!clicked)
        {
            return;
        }
        if (!active)
        {
            this.img.color = activeColor;
            mySeq.sequence[this.index] = true;
            active = true;
        }
        else
        {
            this.img.color = inactiveColor;
            mySeq.sequence[this.index] = false;
            active = false;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        clicked = true;
        if (!active)
        {
            this.img.color = activeColor;
            mySeq.sequence[this.index] = true;
            active = true;
        }
        else
        {
            this.img.color = inactiveColor;
            mySeq.sequence[this.index] = false;
            active = false;
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        clicked = false;
    }
}

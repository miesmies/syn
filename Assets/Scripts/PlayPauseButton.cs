﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayPauseButton : MonoBehaviour {

    private LegacySeq seq;
    private Image img;

    public Sprite play;
    public Sprite pause;
    public bool paused = false;

    //prevent flood
    private float floodTimer = 0.3f;
    private bool floodBlock = false;

    private void Start()
    {
        img = GetComponent<Image>();
        seq = GameObject.Find("GameManager").GetComponent<LegacySeq>();
        
    }

    public void PauseSequencer()
    {
       
        if (floodBlock)
        {
            return;
        }
        if (paused)
        {
            img.sprite = pause;
            paused = false;
            
            //seq.pause = false;
            //seq.ResumePlay();
            StartCoroutine(FloodCheck());
        }
        else
        {
            img.sprite = play;
            paused = true;
            
            //seq.pause = true;
            StartCoroutine(FloodCheck());
        }
    }

    public IEnumerator FloodCheck()
    {
        floodBlock = true;

        yield return new WaitForSeconds(floodTimer);
        floodBlock = false;
    }
}
       



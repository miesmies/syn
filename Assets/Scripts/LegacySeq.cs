﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FMODUnity;
using FMOD;

public class LegacySeq : MonoBehaviour
{

    public Slider slider;
    public bool pause;

    public Transform buttonParent;
    public GameObject button;

    public float step = 0.25f;
    public float step_t;

    public static int currentIndex = 0;

    public static SoundScape currentSoundScape;
    private List<FMOD_StudioEventEmitter> sfx = new List<FMOD_StudioEventEmitter>();
    private ColorPickers colors;

    public List<Note> notes = new List<Note>(); //this is legacy, used for clearing notes
    public List<List<Note>> oNotes = new List<List<Note>>();

    public delegate void OnStepHandler();
    public static event OnStepHandler OnStep;

    [SerializeField]
    private SequencerDriver seqDriver;
   
    private void Start()
    {

        
    
        pause = true;
        colors = GetComponent<ColorPickers>();
        currentIndex = 0;
       
        currentSoundScape = GameObject.Find("SoundScape_01").GetComponent<SoundScape>();
        for (int i = 0; i < 21; i++)
        {
            return;
            //FMOD_StudioEventEmitter s = gameObject.AddComponent<FMOD_StudioEventEmitter>();
       
            //sfx.Add(s);
            for (int n = 0; n < 16; n++)
            {
                oNotes.Add(new List<Note>());
                GameObject btn = Instantiate(button, buttonParent, false);
                Note note = btn.GetComponent<Note>();
                if (i < 5)
                {
                    btn.GetComponent<Image>().color = colors.drumColor;
                    note.inactiveColor = colors.drumColor;
                }
                else if (i < 13)
                {
                    btn.GetComponent<Image>().color = colors.midColor;
                    note.inactiveColor = colors.midColor;
                }
                else
                {
                    btn.GetComponent<Image>().color = colors.highColor;
                    note.inactiveColor = colors.highColor;
                }
                
                note.activeColor = colors.enabledColor;
                
                note.index = n;
                note.row = i;
                notes.Add(note);

                oNotes[n].Add(note);
                
            }
            
        }
        step_t = step;
        
        StartCoroutine(Init());
        
    }

    public List<Note> CreateNotes(int row, Sequencer seq)
    {
        List<Note> tempList = new List<Note>();
        for (int i = 0; i < 16; i++)
        {
            GameObject btn = Instantiate(button, buttonParent, false);
            Note note = btn.GetComponent<Note>();
            note.index = i;
            note.row = row;
            note.mySeq = seq;
            tempList.Add(note);
        }
        return tempList;
    }

    IEnumerator Init()
    {
        yield return new WaitForSeconds(1);
        //pause = false;
    }

    public int stepCounter = 0;
    //public bool oneRow = false;
    //private void FixedUpdate()
    //{
        
    //    if (pause)
    //    {
    //        return;
    //    }
    //    stepCounter++;
    //    //UnityEngine.Debug.Log(stepCounter);
    //    if (/*step_t < 0*/ stepCounter % step == 0)
    //    {
            

    //        foreach (Note n in oNotes[currentIndex])
    //        {
    //            if (n.index == currentIndex)
    //            {
                    
    //                if (n.active)
    //                {
    //                    if (OnStep != null)
    //                    {
    //                        OnStep();
    //                    }
    //                    //if (sfx[n.row].clip != currentSoundScape.kit[n.row])
    //                    //{
    //                    //    sfx[n.row].clip = currentSoundScape.kit[n.row];
    //                    //}
    //                    //sfx[n.row].pitch = Random.Range(0.98f, 1.02f);
    //                    //sfx[n.row].Stop();

    //                    //sfx[n.row].Play();
    //                    //RuntimeManager.PlayOneShot("event:/" + currentSoundScape.soundScapeName + "/" + currentSoundScape.kitString[n.row]);

    //                    //n.Flash();
    //                }
    //                else
    //                {
    //                    n.HighLight();
    //                }
    //            }
    //        }

    //        currentIndex++;
    //        if (currentIndex == 16)
    //        {
    //            currentIndex = 0;
    //        }
    //        //step_t = step;
    //        //UnityEngine.Debug.Log(step_t);
    //    }
    //    //step_t -= Time.deltaTime;
        
    //}

    //public void ResumePlay()
    //{
    //    StartCoroutine(Step());
    //}

    
    //public IEnumerator Step()
    //{
      
    //    foreach(Note n in notes)
    //    {
    //        if(n.index == currentIndex)
    //        {
    //            if (n.active)
    //            {
    //                sfx.PlayOneShot(currentSoundScape.kit[n.row]);
    //                n.Flash();
    //            }else
    //            {
    //                n.HighLight();
    //            }
    //        }
    //    }
        
    //    currentIndex++;
    //    if(currentIndex == 16)
    //    {
    //        currentIndex = 0;
    //    }
    //    yield return new WaitForSeconds(step);
    //    if (!pause)
    //    {

    //        StartCoroutine(Step());
    //    }
        
    //}

    public void ClearNotes()
    {
        foreach (Note n in notes)
        {
            if (n.active)
            {
                n.DeactivateNote();
            }
        }
    }

    public void AdjustStep()
    {
        step = (int)slider.value;
    }
}

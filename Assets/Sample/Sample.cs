﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Collections;

public class Sample : MonoBehaviour
{
    #region Enumerations

    #endregion

    #region Events and Delegates

    #endregion

    #region Variables
    public SoundScape startKit;
    public LegacySeq legacySeq;
    public GameObject seqObj;
    public GameObject seqParent;
    public SequencerDriver seqDriver;
    public SequencerBase sequnncerBase;
    public Slider slider;
    public Text playingText;
    public Text bpmText;
    public Text mutedText;
    public bool pause;
    #endregion

    #region Properties

    #endregion

    #region Methods

    private List<Sequencer> sequencers = new List<Sequencer>();

    private void Awake()
    {
        //playingText.text = "Playing: " + sequnncerBase.IsPlaying;
        //mutedText.text = "Muted: " + sequnncerBase.isMuted;
        //bpmText.text = /*"Bpm: " +*/ sequnncerBase.bpm.ToString();
    }

    private void Start()
    {
        for(int i = 0; i < 21; i++)
        {
            GameObject seq = Instantiate(seqObj, seqParent.transform) as GameObject;
            
            Sequencer s = seq.GetComponent<Sequencer>();
            sequencers.Add(s);
            s.row = i;
            seqDriver.sequencers[i] = s;
        }
        
        StartCoroutine(StartPlay());
        ChangeBpm();
    }

    public void LoadSoundScape(SoundScape soundScape)
    {
        foreach(Sequencer s in seqDriver.sequencers)
        {
            
            s.SetAudioClip(soundScape.kit[s.row]);
        }
    }

    IEnumerator StartPlay()
    {
        yield return new WaitForSeconds(1);
        LoadSoundScape(startKit);
        sequnncerBase.Play();
    }

    public void OnPlay()
    {
        sequnncerBase.Play();
        //playingText.text = "Playing: " + sequnncerBase.IsPlaying;
    }

    public void OnStop()
    {
        sequnncerBase.Stop();
        //playingText.text = "Playing: " + sequnncerBase.IsPlaying;
    }

    private bool floodBlock = false;
    public void TogglePause()
    {
        if (floodBlock)
        {
            return;
        }
        if (pause)
        {
            //legacySeq.pause = false;
            foreach (Sequencer s in sequencers)
            {
                s.Play();
            }
            OnUnPause();
            StartCoroutine(FloodCheck());
            //Time.timeScale = 1;
        }
        else
        {
            //legacySeq.pause = true;
            foreach(Sequencer s in sequencers)
            {
                s.Stop();
            }
            OnPause();
            StartCoroutine(FloodCheck());
            //Time.timeScale = 0;
        }
    }

    public IEnumerator FloodCheck()
    {
        floodBlock = true;

        yield return new WaitForSeconds(0.3f);
        floodBlock = false;
    }

    public void OnPause()
    {
        sequnncerBase.Pause(true);
        pause = true;
        //playingText.text = "Playing: " + sequnncerBase.IsPlaying;
    }

    public void OnUnPause()
    {
        sequnncerBase.Pause(false);
        pause = false;
        //playingText.text = "Playing: " + sequnncerBase.IsPlaying;
    }

    public void OnMute()
    {
        sequnncerBase.Mute(true);
        //mutedText.text = "Muted: " + sequnncerBase.isMuted;
    }

    public void OnUnMute()
    {
        sequnncerBase.Mute(false);
        //mutedText.text = "Muted: " + sequnncerBase.isMuted;
    }

    public void ChangeBpm(/*int bpmDelta*/)
    {
        //sequnncerBase.SetBpm(sequnncerBase.bpm + bpmDelta);
        
        sequnncerBase.SetBpm((int)slider.value);
        bpmText.text = /*"Bpm: " +*/ sequnncerBase.bpm.ToString();
    }

    #endregion

    #region Structs

    #endregion

    #region Classes

    #endregion
}